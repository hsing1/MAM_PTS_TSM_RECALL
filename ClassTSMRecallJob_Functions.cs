﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_TSM_RECALL
{
    public partial class ClassTSMRecallJob
    {
        private void fnProcessStatusINITIAL()
        {
            // 檢查要求 RECALL 的字串格式，如果不符合 TSM 的路徑規則，代表是我們的「後門」符號，直接通過即可；但如果符合了 TSM 路徑規則，就要從嚴處理
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TsmPath(this.ProcessFilePath))
                this.CurrentStaus = ProcessStatus.FINISH;   // 通過
            else
            {
                fnGetTsmFileStatus();                       // 取得檔案屬性，並轉譯成這個物件要用的enum狀態
                for (int i = 0; i < 5; i++)
                {
                    if (this.CurrentStaus != ProcessStatus.ERROR)
                        break;
                    // 等一下再進行
                    for (int j = 0; j < 10; j++)
                        System.Threading.Thread.Sleep(1000);
                    fnGetTsmFileStatus();
                }
            }

            // 僅需列出 fnGetTsmFileStatus 會發生的
            switch (this.CurrentStaus)
            {
                // 在 fnUpdateDbStatus 中如果發生錯誤，會自動將狀態重置為 INITIAL
                // 所以必需要將資料庫狀態更新且正確後，才可以執行後續的動作

                case ProcessStatus.FINISH:
                    // 先修改資料庫，如果成功的話，再發送 HTTP POST
                    // 如果 HTTP POST 發送失敗的話，就 ... 斷頭了 =_="
                    if (fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.SUCCESS))
                        fnProcessStatusFINISH();                // HTTP POST
                    break;

                case ProcessStatus.TAPEONLY:
                    if (fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.RECALLING))
                    {
                        fnProcessStatusTAPEONLY();              // 產生Thread去做TSMRecall的動作
                        CurrentStaus = ProcessStatus.DO_RECALL; // Tricky
                    }
                    break;

                case ProcessStatus.OFFLINE:
                    if (fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.WAIT_CHECKIN))
                    {
                        fnProcessStatusWAIT_TAPE_CHECKIN();
                        CurrentStaus = ProcessStatus.WAIT_TAPE_CHECKIN;
                    }
                    break;

                case ProcessStatus.NOT_AVILABLE:
                    if (fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.NOT_EXIST))
                        fnProcessStatusNOT_AVILABLE();          // HTTP POST
                    break;

                default:
                    string errMsg = string.Concat("異常狀態：", CurrentStaus.ToString());
                    if (fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.SYSERROR, errMsg))
                        fnProcessStatusERROR();                 // HTTP POST
                    break;
            }
        }

        // 查詢磁帶編號 + 查詢磁帶是否在磁帶櫃中
        private void fnProcessStatusWAIT_TAPE_CHECKIN()
        {
            // 如果沒有查過磁帶編號，就先查吧!
            if (this.VolumeName.Count == 0)
                // 這邊如果有查到時，會順便寫入自己的屬性
                if (!fnGetObjectVolumeNames())
                {
                    // 查詢失敗就宣告錯誤
                    CurrentStaus = ProcessStatus.ERROR;
                    fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.SYSERROR, "fnGetObjectVolumeNames() 回傳的結果為 false，請查詢前一筆錯誤記錄");
                    return;
                }

            // 檢查該路徑所屬的磁帶是否有在磁帶櫃中
            if (fnCheckIfTapeExist())
            {
                // 如果有的話，就從頭來檢查 :D
                CurrentStaus = ProcessStatus.INITIAL;
                fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.INITIAL);
            }
        }

        // 拋出執行緒，進行 dsmrecall 的動作
        private void fnProcessStatusTAPEONLY()
        {
            // 因為接下來的動作時間相當長，所以拋出執行緒來做這件事
            objCurrentThread = new System.Threading.Thread(this.fnDoTsmFileRecall);
            objCurrentThread.Start();
        }

        // 沒做事
        private void fnProcessStatusDO_RECALL()
        {
            // 理論上是不用做事
        }

        // ===================================================================

        private void fnProcessStatusERROR()
        {
            string msg = string.Format("錯誤，檔名 = {0}", this.ProcessFilePath);
            string postMsg = string.Empty;

            try
            {
                SR_TSMRecallService.TSMRecallServiceSoapClient obj = new SR_TSMRecallService.TSMRecallServiceSoapClient();
                postMsg = obj.CreateTsmPostMessage(TsmRecallJobID, SR_TSMRecallService.TsmRecallJobResult.SYSTEM_ERROR, msg);
                obj.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("執行 SR_TSMRecallService.CreateTsmPostMessage 時發生錯誤，參數 = {0}，錯誤訊息 = {1}", msg, ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("/TSMRecallAp/fnProcessStatusERROR", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                // 這邊不能 return ，不然後面就斷掉了
            }
            GC.Collect();

            // 發送(錯誤的)HTTP POST通知
            fnSendFinishNotify(postMsg);
        }

        // 
        private void fnProcessStatusNOT_AVILABLE()
        {
            string msg = string.Format("Error: File '{0}' is not available.", this.ProcessFilePath);
            string postMsg = string.Empty;

            SR_TSMRecallService.TSMRecallServiceSoapClient obj = null;
            try
            {
                obj = new SR_TSMRecallService.TSMRecallServiceSoapClient();
                postMsg = obj.CreateTsmPostMessage(TsmRecallJobID, SR_TSMRecallService.TsmRecallJobResult.FILE_NOT_EXIST, msg);
                obj.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("執行 SR_TSMRecallService.CreateTsmPostMessage 時發生錯誤，參數 = {0}，錯誤訊息 = {1}", msg, ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("/TSMRecallAp/fnProcessStatusNOT_AVILABLE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                // 這邊不能 return ，不然後面就斷掉了
            }
            GC.Collect();

            // 發送(錯誤的)HTTP POST通知
            fnSendFinishNotify(postMsg);
        }

        // 
        private void fnProcessStatusFINISH()
        {
            string msg = string.Format("Finish Job: '{0}'.", this.ProcessFilePath);
            string postMsg = string.Empty;

            // 交由 WebMethod 組合出成功的 XML 內容
            try
            {
                SR_TSMRecallService.TSMRecallServiceSoapClient objSR = new SR_TSMRecallService.TSMRecallServiceSoapClient();
                postMsg = objSR.CreateTsmPostMessage(TsmRecallJobID, SR_TSMRecallService.TsmRecallJobResult.FINISH, msg);
                objSR.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("執行 SR_TSMRecallService.CreateTsmPostMessage 時發生錯誤，參數 = {0}，錯誤訊息 = {1}", msg, ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("/TSMRecallAp/fnProcessStatusFINISH", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                // 這邊不能 return ，不然後面就斷掉了
            }
            GC.Collect();

            // 發送(錯誤的)HTTP POST通知
            fnSendFinishNotify(postMsg);
        }

        // ===================================================================

        /// <summary>
        /// 更新資料庫中的狀態
        /// </summary>
        /// <param name="newStatus"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public bool fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus newStatus, string errMessage = "")
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSWORKER_NAME", TSMRECALL.WorkerHostname);
            sqlParameters.Add("FNTSMJOB_ID", this.TsmRecallJobID);
            sqlParameters.Add("FNSTATUS", ((int)newStatus).ToString());
            sqlParameters.Add("FSRESULT", errMessage);

            // 如果沒辦法回寫資料庫的話，就回 INITIAL，之後可以再輪迴 =_=
            // 但如果一直發生 Error 或 DoRecall 事件的話，就真的很糗了
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TSMJOB_JOBSTATUS", sqlParameters, "system"))
            {
                CurrentStaus = ProcessStatus.INITIAL;
                return false;
            }

            return true;
        }

        // ===================================================================

        // 
        private bool fnDeleteTapeNeedOnline(string tapeSerial, string action = "-1")
        {
            return fnInsertTapeNeedOnline(tapeSerial, "1");
        }

        // 
        private bool fnInsertTapeNeedOnline(string tapeSerial, string action = "1")
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVOLUME_NO", tapeSerial);
            sqlParameters.Add("ACTION", action);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTSM_NEEDCHECKIN", sqlParameters, "system");
        }
    }
}

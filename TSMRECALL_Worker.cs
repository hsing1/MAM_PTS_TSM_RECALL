﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_TSM_RECALL
{
    public partial class TSMRECALL
    {
        public static bool RegisterWorkerInfo()
        {
            // 不需要太過頻繁的更新員工資訊
            if (((TimeSpan)(DateTime.Now - lastRegisterWorkerInfo)).TotalSeconds < 60)
                return true;
            lastRegisterWorkerInfo = DateTime.Now;

            // 註冊員工資訊
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSWORKER_NAME", WorkerHostname);
            sqlParameters.Add("MAX_IDLE_TIME", WORKER_MAX_IDLE_MINUTES.ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TSMJOB_WORKER", sqlParameters, "system", false))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/RegisterWorkerInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_U_TSMJOB_WORKER 時發生錯誤");
                return false;
            }
            return true;
        }

        public static bool UnregisterWorkerInfo()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSWORKER_NAME", WorkerHostname);
            sqlParameters.Add("MAX_IDLE_TIME", WORKER_MAX_IDLE_MINUTES.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TSMJOB_UNREG_WORKER", sqlParameters, "system");
        }

        public static bool GetAssignedWorker(out bool flagActiveWorker)
        {
            // 指派預設值
            flagActiveWorker = false;

            // 從資料庫中找出被指派的WORKER
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TSMJOB_GETWORKER", new Dictionary<string, string>(), out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/GetAssignedWorker", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_Q_TSMJOB_GETWORKER 時發生錯誤");
                return false;
            }

            // 檢查回傳的資料是不是自己
            if (resultData[0]["FSWORKER_NAME"] == WorkerHostname)
                flagActiveWorker = true;

            return true;
        }
    }
}
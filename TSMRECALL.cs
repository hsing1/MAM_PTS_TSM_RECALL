﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;

namespace MAM_PTS_TSM_RECALL
{
    public partial class TSMRECALL : Form
    {
        // TODO LIST
        // 磁帶需上架的內容 (MTD)
        // 發信通知的部份 (延後)
        // private bool fnGetAllJobList() 這部份，其實會與 Reset 的機制有關，要注意

        public const int SYSTIMER_BUSY_INTERVAL = 5 * 1000;         // ms
        public const int SYSTIMER_IDLE_INTERVAL = 10 * 1000;        // ms
        public const int SYSTIMER_NOTMASTER_INTEVAL = 60 * 1000;    // ms
        public const int WORKER_MAX_IDLE_MINUTES = 3;               // min

        // 務必為 public，在每次更新資料庫時要做身份驗證用
        public static string WorkerHostname = System.Windows.Forms.SystemInformation.ComputerName;

        // 系統計時器
        private static System.Windows.Forms.Timer sysTimer = new System.Windows.Forms.Timer();

        // 要執行的佇列
        private static Dictionary<string, ClassTSMRecallJob> processList = new Dictionary<string, ClassTSMRecallJob>();

        // 記錄上一次的工作時間，避免間隔太短
        private static DateTime lastRegisterWorkerInfo = DateTime.MinValue;
        private static DateTime lastWorkingTime = DateTime.MinValue;
        private static DateTime lastSendNotifyMessage = DateTime.MinValue;
        private static DateTime lastGetJob = DateTime.MinValue;

        // 記錄是否正在進行自動運作
        private static bool flagInitial;
        private static bool flagExecute;

        // ========================================================

        public TSMRECALL()
        {
            InitializeComponent();

            // 初始化變數
            flagInitial = true;

            // 取得TSM主機資訊
            flagInitial = fnCreateTSMServerInfo();

            // 停止系統運作
            fnTurnAutoSystemOff();
        }

        // ========================================================

        private void TSMRECALL_Load(object sender, EventArgs e)
        {
            // 設定計時器
            sysTimer.Enabled = false;
            sysTimer.Tick += new EventHandler(SysTimerEventProcessor);
            sysTimer.Interval = 1000;

            // 如果初始化沒問題，就自動啟用系統
            if (flagInitial)
                fnTurnAutoSystemOn();
        }

        private void TSMRECALL_FormClosing(object sender, FormClosingEventArgs e)
        {
            fnTurnAutoSystemOff();
        }

        // ========================================================

        // 每 n 秒就會被呼叫的 routine 工作
        private void SysTimerEventProcessor(object sender, EventArgs e)
        {
            // 把計時器停住，讓中間過程不列入TIMER計時，如果過程中有卡住
            // 就不會再有 REGISTER_WORKER_INFO 的事件處理
            // 資料庫就不會有新的 WORKER_UPDATETIME
            // 於是另一個人就有機會接手了
            // 註：要注意的是卡太久後，舊的員工又回神了，但新員工已接手!
            //     所以，在要做「更新」、「寫入」之前，都最好要檢查一下自己的身份

            // 更新畫面顯示
            fnUpdateLastCheckTime();

            // 計時器暫停
            sysTimer.Enabled = false;
            if (DateTime.Now.Subtract(lastGetJob).TotalSeconds < 10 * 60)
                sysTimer.Interval = SYSTIMER_BUSY_INTERVAL;
            else
                sysTimer.Interval = SYSTIMER_IDLE_INTERVAL;

            // 註冊 WORKER_INFO
            RegisterWorkerInfo();

            // 檢查自己是不是「真命天子」，如果不是->就此中斷
            bool flagMasterWorker;
            GetAssignedWorker(out flagMasterWorker);

            // 更新畫面顯示
            fnUpdateMasterWorker(flagMasterWorker);

            // 判斷一下是否要繼續進行下去
            if (!flagMasterWorker)
            {
                sysTimer.Interval = SYSTIMER_NOTMASTER_INTEVAL;
                sysTimer.Enabled = true;
                return;
            }

            // ------- 真命天子才能做以下的事 -------

            // 撈取本次的工作，並放沒有重複的項目加到工作佇列裡
            fnGetAllJobList();
            Application.DoEvents();

            // 更新畫面的呈現(執行中的檔案數)
            fnUpdateProcessCount(processList.Count);

            // 如果沒有工作要處理的話，就在這邊中止
            if (processList.Count == 0)
            {
                sysTimer.Enabled = true;
                return;
            }
            else
                lastGetJob = DateTime.Now;

            // 更新畫面的顯示
            fnUpdateLastExecTime();

            // 針對每一筆資料做處理
            foreach (ClassTSMRecallJob obj in processList.Values)
            {
                // 任務處理
                fnProcessJob(obj);
                Application.DoEvents();

                // 更新註冊資訊
                RegisterWorkerInfo();       // 有 Timer 保護著，不用怕太容易被觸發
                Application.DoEvents();
            }

            // 清除成功的部份
            fnRemoveFinishJob();

            // 更新畫面的呈現(執行中的檔案數)
            fnUpdateProcessCount(processList.Count);

            // 重新打開TIMER，期待下一次的任務
            sysTimer.Enabled = true;
        }

        // ========================================================

        private void button1_Click(object sender, EventArgs e)
        {
            if (!flagInitial)
            {
                MessageBox.Show("初始化程式時發生錯誤，請確認網路環境後，再重開本程式。");
                return;
            }

            Button objBtn = (Button)sender;
            objBtn.Enabled = false;
            if (flagExecute)
                fnTurnAutoSystemOff();
            else
                fnTurnAutoSystemOn();
            objBtn.Enabled = true;
        }

        // ========================================================

        private void fnUpdateMasterWorker(bool isMasterWorker)
        {
            if (isMasterWorker)
                textBox1.Text = "是";
            else
                textBox1.Text = "否";
            Application.DoEvents();
        }

        private void fnUpdateLastExecTime()
        {
            textBox2.Text = DateTime.Now.ToString();
            Application.DoEvents();
        }

        private void fnUpdateLastCheckTime()
        {
            textBox3.Text = DateTime.Now.ToString();
            Application.DoEvents();
        }

        private void fnUpdateProcessCount(int count)
        {
            textBox4.Text = count.ToString();
            Application.DoEvents();
        }
    }
}

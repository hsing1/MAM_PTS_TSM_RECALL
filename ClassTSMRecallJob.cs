﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace MAM_PTS_TSM_RECALL
{
    public partial class ClassTSMRecallJob
    {
        // 可能會發生的狀態列表：
        // 一定要維持：FINISH為0、INITIAL為1、Error的部份為負值，這樣可以讓刪除清單的程式邏輯比較簡單一點 ( < 1 ) <- fnRemoveFinishJob 的邏輯
        // note: Nearline的部份似乎變成虛設了!
        public enum ProcessStatus { ERROR = -2, NOT_AVILABLE = -1, FINISH = 0, INITIAL = 1, OFFLINE, WAIT_TAPE_CHECKIN, TAPEONLY, DO_RECALL, NEARLINE };

        public static List<string> TSM_RSH_Arug = new List<string>();

        //public static string HsmCommandServer;
        //public static string HsmCommandServerUser;
        //public static string HsmCommandServerPass;

        public string TsmRecallJobID;
        public string ProcessFilePath;      // 要處理的檔案路徑(TSM路徑)
        public string NotifyAddress;        // 當完成後要發送訊息的位置(可為空白字串)

        public ProcessStatus CurrentStaus;  // 當下的任務進度
        public string StartProcessTime;     // 開始處理的起始時間
        public List<string> VolumeName;     // 所屬的磁帶編號(可為空白字串)，並記錄該磁帶是否帶架上

        // 要執行 dsmrecall 動作時所需要的東西
        public System.Threading.Thread objCurrentThread = null;
        public System.Diagnostics.Process objCurrentProcess = null;

        // 
        public ClassTSMRecallJob()
        {
            // 變數初始化
            this.CurrentStaus = ProcessStatus.INITIAL;
            this.StartProcessTime = System.DateTime.Now.ToString();
            this.VolumeName = new List<string>();
        }

        // 
        public void GotoNextStatus()
        {
            // 照理來說，被 mark 掉的，都是在這邊不會出現的狀況才對
            switch (CurrentStaus)
            {
                //case ProcessStatus.ERROR :
                //    fnProcessStatusERROR();
                //    return;
                //case ProcessStatus.NOT_AVILABLE :
                //    fnProcessStatusNOT_AVILABLE();
                //    return;
                case ProcessStatus.INITIAL :
                    fnProcessStatusINITIAL();
                    break;
                //case ProcessStatus.OFFLINE :
                //    fnProcessStatusOFFLINE();
                //    break;
                case ProcessStatus.WAIT_TAPE_CHECKIN :
                    fnProcessStatusWAIT_TAPE_CHECKIN();
                    break;
                //case ProcessStatus.TAPEONLY :
                //    fnProcessStatusTAPEONLY();
                //    break;
                case ProcessStatus.DO_RECALL :
                    fnProcessStatusDO_RECALL();
                    break;
                //case ProcessStatus.NEARLINE :
                //    fnProcessStatusNEARLINE();
                //    break;
                //case ProcessStatus.FINISH :
                //    fnProcessStatusFINISH();
                //    return;
                default:
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/GotoNextStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, "Unknown Status = " + CurrentStaus.ToString());
                    break;
            }
            return;
        }

        #region TSM_Opeartion_Actions_Function
        /// <summary>
        /// 呼叫 TSM 的 WebMethod 取得檔案屬性，然後再轉成我們需要用的狀態
        /// </summary>
        /// <returns></returns>
        private void fnGetTsmFileStatus()
        {
            DateTime timeStart = DateTime.MinValue;
            DateTime timeEnd = DateTime.MinValue;

            // 呼叫 TSM_WebMethod，取得檔案屬性
            SR_ServiceTSM.ServiceTSMSoapClient objAPI = null;
            SR_ServiceTSM.TSM_File_Status tmpStatus = SR_ServiceTSM.TSM_File_Status.SysError;
            try
            {
                // 因為發現這邊的 Timeout 太過多了，檢查看看到底是啥問題
                // 查詢 SR_ServiceTSM.GetFileStatusByTsmPath 時發生錯誤：
                // System.TimeoutException: 要求通道在嘗試傳送時於 00:01:00 之後逾時。
                // 請增加傳送至要求呼叫的逾時值，或增加繫結上的 SendTimeout 值。
                // 分配給此作業的時間可能是較長逾時的一部分。 
                // ---> System.TimeoutException: 對 'http://mam.pts.org.tw/WS/WSTSM/WSTSM.asmx' 的 HTTP 要求已超出分配的逾時 00:01:00。分配給此作業的時間可能是較長逾時的一部分。 
                // ---> System.Net.WebException: 作業逾時     於 System.Net.HttpWebRequest.GetRequestStream(TransportContext& context)     於 System.Net.HttpWebRequest.GetRequestStream()     於 System.ServiceModel.Channels.HttpOutput.WebRequestHttpOutput.Ge

                timeStart = DateTime.Now;
                
                objAPI = new SR_ServiceTSM.ServiceTSMSoapClient();
                tmpStatus = objAPI.GetFileStatusByTsmPath(this.ProcessFilePath);
                objAPI.Close();

                timeEnd = DateTime.Now;

                // 純記錄
                if (timeEnd.Subtract(timeStart).TotalSeconds > 10)
                {
                    string errMsg = string.Concat("查詢 SR_ServiceTSM.GetFileStatusByTsmPath 時間異常：timeS = ", timeStart, ", timeE = ", timeEnd);
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/fnGetTsmFileStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, errMsg);
                }
            }
            catch (Exception ex)
            {
                // 記錄，預設本來就是 SR_ServiceTSM.TSM_File_Status.SysError
                string errMsg = string.Concat("查詢 SR_ServiceTSM.GetFileStatusByTsmPath 發生錯誤：timeS = ", timeStart, ", timeE = ", timeEnd, ", msg = ", ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/fnGetTsmFileStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
            }

            // 將取得的資訊做簡單的轉換
            switch (tmpStatus)
            {
                case SR_ServiceTSM.TSM_File_Status.NearLine:
                    CurrentStaus = ProcessStatus.FINISH;
                    break;
                case SR_ServiceTSM.TSM_File_Status.TapeOnly:
                    CurrentStaus = ProcessStatus.TAPEONLY;
                    break;
                case SR_ServiceTSM.TSM_File_Status.TapeOffline:
                    CurrentStaus = ProcessStatus.OFFLINE;
                    break;
                case SR_ServiceTSM.TSM_File_Status.NotAvailable:
                    CurrentStaus = ProcessStatus.NOT_AVILABLE;
                    break;
                // 其他的狀態都列為 ERROR
                default:
                    CurrentStaus = ProcessStatus.ERROR;
                    break;
            }
        }

        // 取得並回寫物件的Volume屬性值，有可能跨捲，所以用List<string>
        private bool fnGetObjectVolumeNames()
        {
            // 查詢 WebService
            SR_ServiceTSM.ServiceTSMSoapClient objAPI = null;
            SR_ServiceTSM.ArrayOfString arrString = null;
            try
            {
                objAPI = new SR_ServiceTSM.ServiceTSMSoapClient();
                if (!objAPI.GetVolumeNameByTsmPath(this.ProcessFilePath, out arrString) || arrString.Count == 0)
                    return false;
                objAPI.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("執行 SR_ServiceTSM.GetVolumeNameByTsmPath 時發生錯誤，參數 = {0}，錯誤訊息 = {1}", ProcessFilePath, ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/fnGetObjectVolumeNames", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 將回傳的結果通通放回屬性中
            foreach (string volume in arrString)
                this.VolumeName.Add(volume); 

            return true;
        }

        // 檢查該路徑所屬的磁帶是否有在磁帶櫃中，如果依然有 offline 的磁帶就回傳 false
        private bool fnCheckIfTapeExist()
        {
            int tapeOnlineCounter = 0;
            try
            {
                SR_ServiceTSM.ServiceTSMSoapClient objAPI = new SR_ServiceTSM.ServiceTSMSoapClient();
                foreach (string volume in this.VolumeName)
                {
                    SR_ServiceTSM.TSM_Tape_Status tapeSataus = objAPI.CheckIfTapeOnline(volume);
                    // 這樣寫有個壞處：Offline及SysError都是return false，因此看不出來SysError
                    switch (tapeSataus)
                    {
                        case SR_ServiceTSM.TSM_Tape_Status.OffLine:
                            fnInsertTapeNeedOnline(volume);
                            break;
                        case SR_ServiceTSM.TSM_Tape_Status.OnLine:
                            tapeOnlineCounter++;
                            fnDeleteTapeNeedOnline(volume);
                            break;
                        case SR_ServiceTSM.TSM_Tape_Status.SysError:
                            throw new Exception("tapeSataus = SR_ServiceTSM.TSM_Tape_Status.SysError");
                    }
                }
                objAPI.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("執行 SR_ServiceTSM.CheckIfTapeOnline 時發生錯誤，參數 = {0}，錯誤訊息 = {1}", "N/A", ex.Message);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallAPP/fnCheckIfTapeExist", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            
            // 需要全都OnLine才算是OnLine
            if (tapeOnlineCounter == VolumeName.Count)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 執行TSMRecall的動作
        /// </summary>
        private void fnDoTsmFileRecall()
        {
            List<string> result = new List<string>();
            string strCommand = string.Concat("dsmrecall ", this.ProcessFilePath);
            string stderrMsg = string.Empty;
            string resultErr = string.Empty;

            for (int i = 0; i < TSM_RSH_Arug.Count; i++)
            {
                objCurrentProcess = new Process();

                // 以下直接從 TSM WebService 抄來的
                objCurrentProcess.StartInfo.FileName = @"D:\0selfsys\plink.exe";
                // Update：20110222，因為用plink沒辦法載到家目錄的設定檔，所以只好用這個方式來讀
                // Update：20110507，想辦法加入 echo 訊息，讓跳出來的視窗知道是在做啥事
                // Update：20110531，利用sysconfig的讀值做為連線的資訊，並用for迴圈來讓錯誤的程式可以找另一台主機重試
                // Update：20110531，加入輸詢機制，當被選中的主機都沒辦法回應時，才會判斷是錯的，但用這個方法會讓plink視窗被隱藏
                // Update：20111102，取消「冒出來的視窗」，所以連 echo 也可以省了
                //p.StartInfo.Arguments = string.Format(@"{0} "". /root/.bash_profile"";""echo 'TSMRecall {1}'"";""{2}""", TSM_RSH_Arug[0], this.ProcFileTsmPath, strCommand);
                // 
                objCurrentProcess.StartInfo.Arguments = string.Format(@"{0} "". /root/.bash_profile"";""{1}""", TSM_RSH_Arug[0], strCommand);

                objCurrentProcess.StartInfo.UseShellExecute = false;
                objCurrentProcess.StartInfo.CreateNoWindow = true;
                objCurrentProcess.StartInfo.RedirectStandardInput = true;
                objCurrentProcess.StartInfo.RedirectStandardOutput = true;
                objCurrentProcess.StartInfo.RedirectStandardError = true;

                try
                {
                    objCurrentProcess.Start();

                    // 視窗跳出來的邏輯，比較好除錯
                    //while (!p.HasExited)
                    //{
                    //    //
                    //}

                    // 視窗隱藏的邏輯
                    while (!objCurrentProcess.StandardOutput.EndOfStream)
                        result.Add(objCurrentProcess.StandardOutput.ReadLine());
                    stderrMsg = objCurrentProcess.StandardError.ReadToEnd();

                    if (!string.IsNullOrEmpty(stderrMsg))
                        resultErr = string.Format("執行plink得到錯誤的回傳內容：指令 = {0}, 參數 = {1}, 錯誤訊息 = {2}", strCommand, objCurrentProcess.StartInfo.Arguments, stderrMsg);

                    objCurrentProcess.Close();
                    objCurrentProcess.Dispose();
                }
                catch (Exception ex)
                {
                    resultErr = string.Format("執行plink碰到例外狀況：指令 = {0}, 參數 = {1}, 錯誤訊息 = {2}", strCommand, objCurrentProcess.StartInfo.Arguments, ex.Message);
                }

                // 如沒有錯誤訊息，就將狀態改為 INITIAL，等待下一輪的重新檢查
                if (string.IsNullOrEmpty(resultErr))
                {
                    this.CurrentStaus = ProcessStatus.INITIAL;
                    fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.INITIAL);
                    return;
                }
            }

            // 這邊代表所有主機都是沒回應的，只能判為TSMRecallFail，並寫記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallAPP", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, resultErr);

            // 特殊的錯誤狀況：ANS9101I No migrated files matching，碰到這錯誤時也做重置的動作
            if (resultErr.Contains("ANS9101I No migrated files matching"))
            {
                this.CurrentStaus = ProcessStatus.INITIAL;
                fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.INITIAL);
                return;
            }

            // 更新資料狀態時，順便將錯誤的訊息寫入
            fnUpdateDbStatus(SR_TSMRecallService.TsmRecallStatus.RECALL_ERROR, resultErr);

            // 如果發生錯誤時，要發送 HTTP POST
            fnProcessStatusERROR();

            // 這邊是確實發生錯誤，會中斷服務，寄個信讓大家確認一下
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Concat("任務編號：", this.TsmRecallJobID));
            sb.AppendLine(string.Concat("執行指令：", strCommand));
            sb.AppendLine(string.Concat("錯誤訊息：", resultErr));
            MAM_PTS_DLL.Protocol.SendEmail("pts_mam@ftv.com.tw", "TSM Recall發生錯誤", sb.ToString());

            // 這邊要修改狀態，才會被應用程式移掉
            this.CurrentStaus = ProcessStatus.ERROR;

            return;
        }
        #endregion


        // Update：發信需求，改由Jimmy提供的TSMNotifier程式取代
        //private void fnSendCheckinNotify()
        //{
        //    foreach (string volume in this.VolumeName.Keys)
        //    {
        //        // 如果磁帶在架上就不用發信，可能是"另一半"不在架上
        //        if (VolumeName[volume] == true)
        //            continue;

        //        DateTime currentTime = System.DateTime.Now;

        //        // 檢查上一次發信通知的時間點
        //        if (LastSendCheckinNotify.ContainsKey(volume))
        //        {
        //            double timeDiffMinute = currentTime.Subtract(LastSendCheckinNotify[volume]).TotalMinutes;
        //            // 間隔太短就不會重複發信
        //            if (SEND_CHECKIN_NOTIFY_INTERVAL > timeDiffMinute)
        //                continue;
        //        }

        //        // 發信吧!
        //        //MAM_PTS_DLL.Protocol.SendGroupMail(MAM_PTS_DLL.Protocol.enumDEFAULT_USERGROUPS.Admins.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);

        //        // 更新最後一次發信時間
        //        LastSendCheckinNotify[volume] = currentTime;
        //    }
        //}

        private void fnSendFinishNotify(string msg)
        {
            // 如果真的有這個地方的話，抓不到問題 =_="
            if (string.IsNullOrEmpty(this.NotifyAddress))
                return;

            if (!MAM_PTS_DLL.Protocol.SendHttpPostMessage(this.NotifyAddress, msg))
                MAM_PTS_DLL.Log.AppendTrackingLog("APP_TSM_RECALL", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, string.Format("發送HTTP訊息錯誤，目的：{0}，訊息：{1}", this.NotifyAddress, msg));
        }
    }
}
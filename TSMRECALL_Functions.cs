﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;

namespace MAM_PTS_TSM_RECALL
{
    public partial class TSMRECALL
    {
        /// <summary>
        /// 讀取在sysConfig中的TSM主機資訊，如果讀取或解析失敗的話就會將flagInitial設為false
        /// 這段程式其實是從WSTSM底下的fnCreateNewServerObjectList改寫過來的
        /// </summary>
        /// <returns></returns>
        private bool fnCreateTSMServerInfo()
        {
            // 清除舊有的資訊(或是初始化)
            ClassTSMRecallJob.TSM_RSH_Arug.Clear();

            // 讀取 sysConfig 內的資訊
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            XmlNodeList objXmlNodeList = obj.sysConfig_Read_From_NodeList("/ServerConfig/TSM");

            // 開始解析取回的內容
            XmlNode tsmserver, serverAttributes, root;
            root = objXmlNodeList[0];
            IEnumerator ienum = root.GetEnumerator();
            while (ienum.MoveNext())
            {
                tsmserver = (XmlNode)ienum.Current;
                IEnumerator ienumSub = tsmserver.GetEnumerator();
                while (ienumSub.MoveNext())
                {
                    serverAttributes = (XmlNode)ienumSub.Current;
                    if (serverAttributes.Name == "RSH_ARGU" && !string.IsNullOrEmpty(serverAttributes.InnerText))
                        ClassTSMRecallJob.TSM_RSH_Arug.Add(serverAttributes.InnerText);
                }
            }

            // 檢查是否有正確的值讀入
            return ClassTSMRecallJob.TSM_RSH_Arug.Count > 0;
        }

        // ========================================================

        private void fnTurnAutoSystemOn()
        {
            flagExecute = true;

            sysTimer.Interval = 100;
            sysTimer.Enabled = true;

            button1.Text = "自動模式";
            System.Windows.Forms.Application.DoEvents();
        }

        [Obsolete("當程式被強制關閉時，沒有做好相對應的處理，可能會永遠變成「RECALLING」的狀態")]
        private void fnTurnAutoSystemOff()
        {
            sysTimer.Enabled = false;

            flagExecute = false;

            button1.Text = "尚未執行";
            System.Windows.Forms.Application.DoEvents();

            // 刪除員工記錄
            UnregisterWorkerInfo();

            // 初始化變數(重要，不然程式開開關關會沒辦法運作)
            lastRegisterWorkerInfo = DateTime.MinValue;
            lastWorkingTime = DateTime.MinValue;
            lastSendNotifyMessage = DateTime.MinValue;

            // 考慮將執行中的作業都 Reset 掉

            // 清除記憶體
            if (processList != null)
                fnClearQueueMemory();
            else
                processList = new Dictionary<string, ClassTSMRecallJob>();
            GC.Collect();

            // 清除介面上的資訊
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
        }

        // ========================================================

        /// <summary>
        /// 將三天內非成功的任務重置為「Initial」
        /// </summary>
        /// <returns></returns>
        private bool fnInitialJobStatusInDb()
        {
            // 這機制太可怕了，所以我拿掉了
            //if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TSMJOB_INITIALJOBS", new Dictionary<string, string>(), "system"))
            //{
            //    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/fnInitialJobStatusInDb", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error");
            //    return false;
            //}
            //return true;
            return true;
        }

        // 只有在系統中斷時，才可以執行這隻程式
        private void fnClearQueueMemory()
        {
            processList.Clear();
            processList = new Dictionary<string, ClassTSMRecallJob>();
            GC.Collect();
        }

        /// <summary>
        /// 找出所有需要做的工作，並丟進工作佇列
        /// </summary>
        /// <returns></returns>
        private bool fnGetAllJobList()
        {
            // 找出所有狀態為「未結束」的任務
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("INITIAL_VALUE", ((int)SR_TSMRecallService.TsmRecallStatus.INITIAL).ToString());
            sqlParameters.Add("SUCCESS_VALUE", ((int)SR_TSMRecallService.TsmRecallStatus.SUCCESS).ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TSMJOB_GETJOBLIST", sqlParameters, out resultData))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/TSMRecallApp/fnGetAllJobList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_Q_TSMJOB_GETJOBLIST 時發生錯誤回傳");
                return false;
            }

            // 檢替回傳的筆數
            if (resultData.Count == 0)
                return true;

            // 將所有回傳的資料做檢視
            for (int i = 0; i < resultData.Count; i++)
            {
                // 不需加入重複的項目
                if (processList.ContainsKey(resultData[i]["FNTSMJOB_ID"]))
                    continue;

                // 新加入的項目，狀態自動設定為INITIAL
                ClassTSMRecallJob obj = new ClassTSMRecallJob();
                obj.TsmRecallJobID = resultData[i]["FNTSMJOB_ID"];
                obj.ProcessFilePath = resultData[i]["FSSOURCE_PATH"];
                obj.NotifyAddress = resultData[i]["FSNOTIFY_ADDR"];

                // 加入處理清單
                processList.Add(obj.TsmRecallJobID, obj);
            }

            return true;
        }

        /// <summary>
        /// 執行該階段的任務
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool fnProcessJob(ClassTSMRecallJob obj)
        {
            obj.GotoNextStatus();
            return true;
        }

        /// <summary>
        /// 檢查佇列清單，如果是結束(含成功及失敗)的任務就刪除
        /// </summary>
        private void fnRemoveFinishJob()
        {
            List<string> removeJobIdList = new List<string>();

            // 先找出要刪除的JobID
            foreach (string jobID in processList.Keys)
                // 當有錯誤(負值)、完成(0)時，就列入刪除清單
                if ((int)(processList[jobID].CurrentStaus) < 1)
                    removeJobIdList.Add(jobID);

            // 整理工作佇列
            foreach (string jobID in removeJobIdList)
                processList.Remove(jobID);

            // 回收資源
            GC.Collect();
        }
    }
}